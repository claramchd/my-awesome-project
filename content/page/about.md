---
title: À propos
---
Je m'appelle Clara. Depuis très jeune, j'aime cuisiner, une passion qui m'a été transmise par mon papa chef cuisinier. C'est la nostalgie des journées et soirées passées au restaurant, à la plonge et en cuisine, qui me donnent envie de vous partager mes recettes inspirées de celles de mon père. 

***« Il n’y a pas de bonne cuisine si au départ elle n’est pas faite par amitié pour celui ou celle à qui elle est destinée ». - Paul Bocuse***

![Le restaurant de mon père, à Marmande (Lot-et-Garonne)](/restau.jpg)
*Le restaurant de mon père, à Marmande (Lot-et-Garonne)*
