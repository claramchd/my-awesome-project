---
title: "Petite verrine compote spéculoos façon tiramisu"
date: 2019-11-19T21:18:00+02:00
tags: ["facile", "végétarien", "rapide"]
youtube: "hDhU-jHXUcY"

---

![verrine speculoos](/compote.jpg)

### Ingredients 

- 200 g de mascarpone

- 100 g de fromage blanc

- 400 g de compote

- 30 g de sucre

- 9 spéculoos environ (tout dépend de la taille de vos verrines)

- 2 cuillères à café de cannelle

- goutte d'extrait de vanille

### Recette

Mélanger dans un petit saladier la compote avec la cannelle et la vanille.

Mettre une couche de compote dans les verrines, ajouter ensuite une deuxième couche de mascarpone et fromage blanc mélangés.

Réduire en poudre les spéculoos et les saupoudrer à l'envie sur les verrines.

Mettre au frais 4h avant dégustation ou une nuit entière.

### Mes conseils 

Facile et rapide, c'est une de mes recettes phare de l'automne pour régaler les papilles en peu de temps. Petit tip : si vous voulez une recette plus légère, remplacez le mascarpone par du fromage blanc. Elle marche aussi très bien avec de la compote pommes/rhubarbe (meilleure quand faite maison bien sûr)

### Une vidéo pour aller plus loin


