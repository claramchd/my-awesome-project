---
title: Meilleures entrées de saison pour impressionner 
date: 2019-11-10T21:18:00+02:00
tags: ["difficile", "rapide", "long", "facile"]
youtube: "zbI56zmfUdg"

---

Couleurs flamboyantes et saveurs envoûtantes, l’automne avance à grands pas dans les assiettes pour mieux squatter les entrées. Quelle bonne idée ! L’apéritif est terminé, les invités installés et la table encore vide ? Bonne nouvelle, l’automne est là et les produits du terroir s’immiscent au cœur des entrées, pour des recettes aussi parfumées que colorées. Des assiettes gourmandes à savourer bien au chaud, pour mieux fêter le retour d’une saison-cocon !

### Le top 5 des meilleures entrées d'automne

{{< gallery caption-effect="fade" >}}
  {{< figure thumb="-thumb" link="/courge.jpg" caption="courge" alt="Velouté de courge Butternut aux saveurs d'automne" >}}
  {{< figure thumb="-thumb" link="/carpaccio.jpg" caption="carpaccio de cèpes" alt="Carpaccio de cèpes" >}}
  {{< figure thumb="-thumb" link="/gorgonzola.jpg" caption="salade" alt="Salade aux pousses d'epinard, gorgonzola, poires et noix" >}}
{{< /gallery >}}
{{< load-photoswipe >}}

### Une vidéo pour aller plus loin
