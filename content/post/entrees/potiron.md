---
title: "Soupe de potimarron au lait de coco"
date: 2019-10-20T21:18:00+02:00
tags: ["facile", "rapide", "végétarien"]
youtube: "UNw5xBuw4u0"

---

![soupe potimarron](/potimarron.jpg)

### Ingredients 
- 1 potimarron moyen
- 2 carottes
- 2 branches de coriandre
- 1 oignons
- 250 ml de lait de coco
- Sel / poivre
- Une pointe de muscade et de gingembre en poudre
- 1 l d'eau (voire 1,5 L)

### Recette

Éplucher, égrainer et couper le potimarron en morceaux ainsi que les carottes, navets et oignons.
Plonger le tout dans une marmite avec la coriandre, la muscade, le gingembre, sel et poivre et faire cuire 45 min.
Mixer le tout et verser le lait de coco, mélanger, puis déguster...

### Mes conseils
Si votre soupe est trop épaisse, vous pouvez rajouter de l'eau au moment du mixage, jusqu'au résultat souhaité.

### Une vidéo pour aller plus loin
