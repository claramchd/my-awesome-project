---
title: "Lasagnes aux champignons"
date: 2019-11-20T21:18:00+02:00
tags: ["difficile", "long", "végétarien"]

---

![lasagnes aux champignons](/lasagnes.jpg)

### Ingredients 

- 8 feuilles de lasagnes
- 500 g de champignons de Paris
- 2 échalotes
- 500 g de lait végétal
- 30 g de farine
- 50 g de margarine végétale
- 70 g de fromage végétal râpé
- 1 pincée de noix de muscade
- sel, poivre

### Recette

Pelez et émincez les échalotes.

Nettoyez et émincez finement les champignons.

Faites fondre 20 g de margarine dans une poêle.

Faites revenir les échalotes et les champignons jusqu'à ce que l'eau des champignons se soit évaporée.

Salez, poivrez puis retirez du feu.

Faites fondre le restant de margarine dans une casserole.

Ajoutez la farine en pluie et remuez bien.

Versez petit à petit le lait végétal en continuant de fouetter. Mélangez jusqu'à ce que la sauce épaississe.

Ajoutez 50 g de fromage végétal râpé, la noix de muscade puis salez et poivrez.

Préchauffez le four à 200°C.

Placez un peu de sauce béchamel végétale dans le fond d'un plat allant au four.

Recouvrez avec une couche de feuilles de lasagne puis une couche de champignons.

Répétez l'opération jusqu'à épuisement des ingrédients. Terminez par une couche de sauce béchamel.

Parsemez du restant de fromage végétal râpé puis salez et poivrez.

Enfournez pendant 35 minutes jusqu'à ce que les lasagnes soient bien gratinées.
