---
title: "Curry aux patates douces et aux cacahuètes"
date: 2019-11-21T21:18:00+02:00
tags: ["facile", "long", "végétarien"]
youtube: "RwcPc-btgvI"

---

![curry de patates douces](/curry.jpg)

### Ingredients 

- 1 oignon
- 500 g de patates douces
- 2 gousses d’ail
- 3 c. à soupe de curry
- 2 cm de gingembre
- 40 cl de lait de coco
- 25 cl de bouillon de légumes
- 2 c. à soupe d’intenso oignon Mutti
- 100 g de fèves
- 100 g de petits pois
- 100 g de cacahuètes
- 1 citron vert
- 2 branches de coriandre
- sel, poivre

### Recette 

Dans une cocotte, faites chauffer l’huile d’olive et faites revenir l’oignon émincé.

Ajoutez la patate douce coupée en cubes et l’ail écrasé et faites cuire quelques minutes.

Saupoudrez de curry, de gingembre râpé et mélangez bien pour enrober les patates douces.

Ajoutez le lait de coco, le bouillon de légumes et le concentré de tomate, mélangez et poursuivez la cuisson à couvert pendant 15 min, ajoutez les petits pois et poursuivez la cuisson 5 min.

En fin de cuisson ajoutez, le jus du citron vert, les cacahuètes grossièrement concassées et la coriandre ciselée.

### Mes conseils 

Si vous aimez plus épicé, vous pouvez rajouter un petit piment en début de cuisson. J'adore cette recette car on peut la faire avec tous les légumes qui trainent dans le frigo.

### Une vidéo pour aller plus loin 
